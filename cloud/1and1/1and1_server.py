#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) 2016, Carsten Kroll <carsten@ximidi.com>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

DOCUMENTATION = '''
---
module: service
author:
    - Carsten Kroll
version_added: 0.1
short_description:  Manage 1&1 Server via cloud API.
description:
    - Create, start, stop, and destroy server in the 1&1 cloud.
      If ID is not set and state is started then a new instance with the 
      given flavour and appliance (OS) will be created in the supplied datacenter.
      All id's can be obtained with the help of the 1and1.py inventory script.
       
options:
    name:
      required: false
      description:
        - Name of the server
    id:
      required: false
      description:
        - server ID. You can use the 1and1.py dynamic inventory to obtain it.
    state:
      required: true
      choices: [ started, stopped, restarted, absent ]
      description:
        - operational state of the server.
    key:
      required: false
      description:
        - public key for root SSH access.
    flavour:
      required: false
      description:
        - ID of pre configured hardware flavours.
    datacenter:
      required: false
      description:
        - Location where the server is running.
    appliance:
      required: false
      description:
        - ID of image to be provisioned.
'''

#from urllib2 import urlopen
from ansible.module_utils.basic import *
#import urllib2
from ansible.module_utils.urls import *
import json
import os

#ID access to API
TOKEN = os.getenv("API_TOKEN_1AND1")
APIURL = "https://cloudpanel-api.1and1.com/v1/servers"

def sendPUTRequest(url, data):
    _data = json.dumps(data)
    response = open_url(url, _data.encode(encoding='utf_8'), headers={'X-TOKEN':TOKEN, 'content-type':'application/json'}, method="PUT")
    return json.loads(response.read().decode())

def sendGETRequest(url):
    response = open_url(url, headers={'X-TOKEN':TOKEN, 'content-type':'application/json'})
    return json.loads(response.read().decode())

def sendDELETERequest(url):
    response = open_url(url, headers={'X-TOKEN':TOKEN, 'content-type':'application/json'}, method="DELETE")
    return json.loads(response.read().decode())
    
def sendPOSTRequest(url, data):
    _data = json.dumps(data)
    response = open_url(url, _data.encode(encoding='utf_8'), headers={'X-TOKEN':TOKEN, 'content-type':'application/json'}, method="POST")
    return json.loads(response.read().decode())
    
class Server1and1(object):
  """
  """
  _actions = {"started":"POWER_ON", "stopped":"POWER_OFF", "restarted":"REBOOT"}
  _states = {"started":"POWERED_ON", "stopped":"POWERED_OFF"}
  def __init__(self, module):
    self.module     = module
    self.name       = module.params['name']
    self.id         = module.params['id']
    self.state      = module.params['state']
    self.key        = module.params['key']
    self.datacenter = module.params['datacenter']
    self.flavour    = module.params['flavour']
    self.appliance  = module.params['appliance']
    self.changed    = False
    #self.arguments = module.params.get('arguments', '')
  
  
  def _deleteServer(self):
    _command = APIURL + "/" + self.id
    if self.module.check_mode:
      try:
        _changed = True;
        response = sendGETRequest(_command)
      except urllib2.HTTPError as e:
        if ( e.code == 404 ):
          _changed = False
        else:
          response = json.loads(e.read())
          self.module.fail_json(msg=response)
          
      self.module.exit_json(changed=_changed, msg=response) 
      
    else:
      response = sendDELETERequest(_command)
      # wait for server removal
      stat = sendGETRequest(APIURL + "/" + response["id"])
      while ( stat["status"]["state"] == "REMOVING" ):
        stat = sendGETRequest(APIURL + "/" + response["id"])
        time.sleep(10)
      self.module.exit_json(changed=True, msg=response)
    
  def _setStatusServer(self):
    action = self._actions[self.state]

    if self.module.check_mode:
      # in check mode just get the current status and compare it
      # with the desired state
      response = sendGETRequest(APIURL + "/" + self.id)
      _changed = response["status"]["state"] <> self._states[self.state] 
      self.module.exit_json(changed=_changed, msg=response) 
      
    else:
      #Configure the request      
      method = "SOFTWARE"
      content = {'action':action, 'method':method}
      _command = APIURL + "/" + self.id + "/status/action"
      response = sendPUTRequest(_command, content)
      self.module.exit_json(changed=True, msg=response)
  '''
    creates a new server instance
  ''' 
  def _createServer(self):
    #Configure the request
    hardware = { 'fixed_instance_size_id': self.flavour}
    data = {
      'name': self.name,
      'hardware': hardware,
      'appliance_id': self.appliance,
      'datacenter_id': self.datacenter,
      'rsa_key' : self.key
    }
    if self.module.check_mode:
      response = { "create": data }
    else:
      response = sendPOSTRequest(APIURL, data)
      # wait for server provisioning to complete
      response = sendGETRequest(APIURL + "/" + response["id"])
      while ( response["status"]["state"] == "DEPLOYING" ):
        response = sendGETRequest(APIURL + "/" + response["id"])
        time.sleep(10)
        
    self.module.exit_json(changed=True, msg=response)
  '''
    call the appropriate actions
  '''   
  def execute(self):
    try:
      if self.state in ("started", "stopped", "restarted"):
        if (self.id == None and self.state == "started" ): 
          # create mode
          self._createServer()
        elif (self.id == None):
          self.module.fail_json(msg="id is required for state=" + self.state)
          
        self._setStatusServer()
      else:
        if (self.id == None):
          self.module.fail_json(msg="id is required for state=" + self.state)
        self._deleteServer()
      
    except urllib2.HTTPError as e:
      response = json.loads(e.read())
      if ( e.code == 400 ):
        self.module.exit_json(changed=False, msg=response)
      else:
        self.module.fail_json(msg=response)

def main():

  module = AnsibleModule(
    argument_spec = dict(
      id = dict(),
      state = dict(choices=["started", "stopped", "restarted", "absent"], required=True),
      flavour = dict(),
      key = dict(),
      appliance = dict(),
      datacenter = dict(),
      name = dict()
    ),
    supports_check_mode=True
  )
  
  if (not TOKEN):
    module.fail_json(msg="API_TOKEN_1AND1 not set.")
  
  server = Server1and1(module)
  server.execute()
  
if __name__ == '__main__':
  main()